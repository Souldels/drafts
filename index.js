// import Choices from "choices.js";
// если раскоментить типорт, все ломается

// Кастомный селект
function customSelect() {
	const element = document.querySelector('.select');
	const choices = new Choices(element, {
		searchEnabled: false,
		placeholder: false,
		itemSelectText: '',
	});
}

customSelect();

// Яндекс карта
const yaMap = () => {
	ymaps.ready(init);
	function init() {
		var myMap = new ymaps.Map("map", {
			center: [48.8721, 2.3542],
			zoom: 17
		});

		const myPlacemark = new ymaps.Placemark([48.87219247486463, 2.3542102784451577], {}, {
			iconLayout: 'default#image',
			iconImageHref: './img/placemark.png',
			iconImageSize: [48, 48],
		});

		myMap.geoObjects.add(myPlacemark);
	}
}

yaMap();